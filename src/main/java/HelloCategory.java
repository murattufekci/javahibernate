import javax.persistence.*;
import java.io.Serializable;

/**
 * Represents an hello Category
 */
@Entity
public class HelloCategory implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "CAT_DESC")
    private String desc;

    @ManyToOne
    private HelloWorld helloWorld;

    public HelloCategory() {
    }

    public HelloCategory(HelloWorld helloWorld) {
        this.setHelloWorld(helloWorld);
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public HelloWorld getHelloWorld() {
        return helloWorld;
    }

    public void setHelloWorld(HelloWorld helloWorld) {
        this.helloWorld = helloWorld;
    }

    @Override
    public String toString() {
        return "HelloCategory{" +
                "desc='" + desc + '\'' +
                "helloWorld="+helloWorld.getDescription()+
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}