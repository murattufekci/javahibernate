import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "HELLO")
public class HelloWorld implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "HELLO_ID", unique = true, nullable = false, updatable=false)
    private int id;

    @Basic()
    @Column(name = "HELLO_DESC")
    private String Description;

    @OneToMany(orphanRemoval=true, cascade = CascadeType.ALL, mappedBy = "helloWorld")
    private List<HelloCategory> helloCategories = new ArrayList<HelloCategory>();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }


    public void add(HelloCategory helloCategory) {
        //To change body of created methods use File | Settings | File Templates.
        getHelloCategories().add(helloCategory);

    }

    public List<HelloCategory> getHelloCategories() {
        return helloCategories;
    }

    @Override
    public String toString() {
        return "HelloWorld{" +
                "id='" + id + '\'' +
                ", Description='" + Description + '\'' +
                ", helloCategories=" + helloCategories +
                '}';
    }
}