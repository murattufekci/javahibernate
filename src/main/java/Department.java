import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Department implements Serializable {
    @Id @GeneratedValue(strategy=GenerationType.IDENTITY)
    private int id;
    private String name;


    @OneToMany(orphanRemoval=true, cascade = CascadeType.ALL, mappedBy = "department", targetEntity = Student.class)
    @OrderBy("name ASC")
    private List<Person> students;

    public Department() {
        students = new ArrayList<Person>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String deptName) {
        this.name = deptName;
    }

    public void addStudent(Student student) {
        if (!getStudents().contains(student)) {
            getStudents().add(student);
        }
    }

    public List<Person> getStudents() {
        return students;
    }

    @Override
    public String toString() {
        return "HelloWorld{" +
                "id='" + id + '\'' +
                ", Description='" +  name + '\'' +
                ", Students =" + students +
                '}';
    }
}