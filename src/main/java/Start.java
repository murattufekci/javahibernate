import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;

public class Start {
    static EntityManagerFactory emf = Persistence.createEntityManagerFactory("EmployeeService");
    static EntityManager em = emf.createEntityManager();

    public static void main(String[] a) throws Exception {
        // A normal Hello World Construction
        // The normal Hello World
        HelloWorld aNormalHelloWorld = new HelloWorld();
        aNormalHelloWorld.setDescription("Hello Nico !");
        // The normal Hello Category



        // Persistence
        em.getTransaction().begin();
        em.persist(aNormalHelloWorld);

        for(int i=0;i< 10; i++){
            HelloCategory aNormalHelloCategory = new HelloCategory(aNormalHelloWorld);
            aNormalHelloCategory.setDesc("A normal Hello " + i);
            aNormalHelloWorld.add(aNormalHelloCategory);
            em.persist(aNormalHelloWorld);
        }


        em.getTransaction().commit();

        // Retrieve
        // Hello World whose primary key is 1
        HelloWorld helloWorld = em.find(HelloWorld.class, 1);
        System.out.println(helloWorld);
        for (HelloCategory helloCategory:helloWorld.getHelloCategories()){
            System.out.println(helloCategory.getDesc());
        }

        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
        CriteriaQuery<HelloWorld> criteriaQuery = criteriaBuilder.createQuery(HelloWorld.class);
        Root<HelloWorld> root = criteriaQuery.from(HelloWorld.class);

        root.fetch("helloCategories", JoinType.INNER);

        criteriaQuery.where(criteriaBuilder.equal(root.get("id"), 1));

        TypedQuery<HelloWorld> query = em.createQuery(criteriaQuery);

        HelloWorld response = query.getSingleResult();

        System.out.println(response);

        em.getTransaction().begin();
        Department dep = new Department();
        dep.setName("Department 1");
        em.persist(dep);
        for(int i=0;i< 10; i++){
            Student student = new Student();
            student.setName("Student " + i);
            student.setDepartment(dep);
            dep.addStudent(student);
            em.persist(dep);
        }
        em.getTransaction().commit();

        Department department = em.find(Department.class, 1);
        System.out.println(department);

        Student student = em.find(Student.class, 1);
        System.out.println(student);
        em.close();
        emf.close();
    }
}